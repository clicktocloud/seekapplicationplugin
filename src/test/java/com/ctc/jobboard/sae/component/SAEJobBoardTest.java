package com.ctc.jobboard.sae.component;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.ctc.jobboard.sae.domain.Candidate;
import com.ctc.jobboard.sae.domain.application.JobApplication;
import com.ctc.jobboard.sae.domain.application.SAEApplicationResponse;
import com.ctc.jobboard.util.SfOrg;

public class SAEJobBoardTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	
	@Test
	public void testFetchCandidates() {
		SAEAccount account = new SAEAccount(SAEOAuth.TEST_ADVERTISER_ID, "", "29762681");
		account.setOrgMaxContactid("1");
		
		SAEJobBoard jobboard  = new SAEJobBoard("seek");
		jobboard.setAccount(account);
		jobboard.setOperator("Andy");
		
		
		SAECandidatesRetriever retriever = new SAECandidatesRetriever(jobboard);
		List<Candidate> candidates = retriever.fetchCandidates();
		int i = 0;
		for(Candidate c : candidates){
			System.out.println("[ " + i++ + " ]" +c.getJobApplicationId() + " - " + c.getFirstname());
			System.out.println("\t Resume : " + c.getResumeUrl());
			System.out.println("\t Coverletter : " + c.getCoverletterUrl());
			System.out.println("\t Screen["+c.getScreenSize()+"] : " + c.getScreenUrl());
		}
	}
	
	@Test
	public void testFetchScreens(){
		
		SAEAccount account = new SAEAccount(SAEOAuth.TEST_ADVERTISER_ID, "", "29762681");
		SAEJobBoard jobboard  = new SAEJobBoard("seek");
		jobboard.setAccount(account);
		jobboard.setOperator("Andy");
		
		Candidate c = new Candidate();
		c.setScreenUrl("/advertisers/29762681/jobApplications/584404824/screen");
		SAEScreenQuestionsRetriever retriever = (SAEScreenQuestionsRetriever) new SAEScreenQuestionsRetriever(jobboard).setCandidateTypes(Arrays.asList(new Candidate[]{c}));
		retriever.retrieve();
		
	}
	
	@Ignore
	@Test
	public void testFetchCoverlettersandCriteria(){
		
		SAEAccount account = new SAEAccount(SAEOAuth.TEST_ADVERTISER_ID, "", "29762681");
		SAEJobBoard jobboard  = new SAEJobBoard("seek");
		jobboard.setAccount(account);
		jobboard.setOperator("Andy");
		jobboard.setBucket("testbuckt");
		
		Candidate c = new Candidate();
		c.setCoverletterUrl("/advertisers/29762681/jobApplications/584731535/coverLetter");
		SAECoverLettersandSelectionCriteriaRetriever retriever = 
				(SAECoverLettersandSelectionCriteriaRetriever) new SAECoverLettersandSelectionCriteriaRetriever(jobboard).setCandidateTypes(Arrays.asList(new Candidate[]{c}));;
		
				retriever.retrieve();
		
	}
	
	@Test
	public void testFetchAll(){
		
		SAEAccount account = new SAEAccount(SAEOAuth.TEST_ADVERTISER_ID, "", "29762681");
		SAEJobBoard jobboard  = new SAEJobBoard("seek");
		jobboard.setAccount(account);
		jobboard.setOperator("Andy");
		jobboard.setBucket("testbuckt");
		//jobboard.setCurrentSforg(new SfOrg("test"));
		
		jobboard.makeJobFeeds();
		
	}

}
