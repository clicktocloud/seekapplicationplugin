package com.ctc.jobboard.sae.component;

import static org.junit.Assert.assertNotNull;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.ctc.jobboard.sae.component.SAEAccount;
import com.ctc.jobboard.sae.component.SAEOAuth;
import com.ctc.jobboard.sae.utils.SAEConfig;

public class SeekOAuthTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testRetrieveAccessToken() {
		SAEAccount account = new SAEAccount(SAEConfig.getSAEUserName(),SAEConfig.getSAEPassword(),SAEOAuth.TEST_ADVERTISER_ID);
		
		SAEOAuth oauth = new SAEOAuth();
		String token = oauth.retrieveAccessToken(account);
		System.out.println(token);
		assertNotNull(token);
		
	}

}
