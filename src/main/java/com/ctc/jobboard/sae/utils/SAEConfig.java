package com.ctc.jobboard.sae.utils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import com.ctc.common.encryption.coder.AESCoder;
import com.ctc.jobboard.util.BasicConfig;

public class SAEConfig  {
	
	
	private static String decryptedSeekPassword ="";
	
	public static Map<String, Integer> ApiMaxLengthMap = getApiMaxLengthMap();
	
	public static String getSAEUserName(){
		return BasicConfig.get("sae_username");
	}
	
	public static String getSAEPassword(){
		
		synchronized (decryptedSeekPassword) {
			if (StringUtils.isEmpty(decryptedSeekPassword)) {
				decryptedSeekPassword = AESCoder.decrypt(BasicConfig.get("sae_password"));
			}
			return decryptedSeekPassword;
		}
		
	}
	
	public static String getSAEHost(){
		return BasicConfig.get("sae_host");
	}
	
	public static synchronized String getTempFolder(String orgUsername, String bucket) {
		String orgUsernamePrefix = "";
		if(orgUsername.indexOf("@") >= 0)
			orgUsernamePrefix = orgUsername.substring(0, orgUsername.indexOf("@"));
		else{
			orgUsernamePrefix = orgUsername;
		}
		return BasicConfig.get("sae_temp_folder")+orgUsernamePrefix+"_"+bucket+"/";
	}
	
	/**
	 * get max length of all fields
	 * 
	 * @return
	 */
	public static HashMap<String, Integer> getApiMaxLengthMap() {
		HashMap<String, Integer> apiMaxLengthMap = new HashMap<String, Integer>();
		Properties p = new Properties();
		InputStream in = null;
		Set<String> keys = null;

		try {
			// in =
			// Utils.class.getClassLoader().getResourceAsStream("apinameMaxLength.properties");
			in = SAEConfig.class.getClassLoader().getResourceAsStream("config" + File.separator + "apinameMaxLength.conf");
			p.load(in);
			keys = p.stringPropertyNames();
		} catch (IOException e) {
			e.printStackTrace();
		}

		for (String key : keys) {
			Integer length = Integer.parseInt(p.getProperty(key));
			apiMaxLengthMap.put(key, length);
		}
		return apiMaxLengthMap;
	}

}
