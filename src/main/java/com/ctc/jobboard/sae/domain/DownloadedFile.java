package com.ctc.jobboard.sae.domain;

import java.io.File;

import com.ctc.jobboard.core.application.FileType;

public class DownloadedFile implements FileType{
	
	private String orignalFilename;
	private String newFilename;
	private File file;
	public String getOrignalFilename() {
		return orignalFilename;
	}
	public void setOrignalFilename(String orignalFilename) {
		this.orignalFilename = orignalFilename;
	}
	public String getNewFilename() {
		return newFilename;
	}
	public void setNewFilename(String newFilename) {
		this.newFilename = newFilename;
	}
	public File getFile() {
		return file;
	}
	public void setFile(File file) {
		this.file = file;
	}

}
