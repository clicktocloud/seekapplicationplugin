package com.ctc.jobboard.sae.domain;

import java.util.List;

import com.ctc.jobboard.core.application.CandidateType;

public class Candidate implements CandidateType{
	private String orgUsername;
	private String id;
	private String firstname;
	private String lastname;
	private String email;
	private String phone;
	private String jobApplicationId;
	private String jobseekerId;
	private String jobreference;
    private String vacancyId;
    private String advertisementId;
	private String dateSubmitted;
	private String candidateId;
	private String resumeOriginalName;
	private String resumeName;
	private String coverletterOriginalName;
	private String coverletterName;
	private String resumeContent;
	private String resumeSize;
	private String resumePath;
	private String coverletterSize;
    private List<String> skillGroupsList;   // save skill groups criteria
	
	//*************
	private String selectionCriteriaUrl;
	private String screenUrl;	
	private String selectionCriteriaName;
	private String selectionCriteriaOriginalName;
	private String selectionCriteriaSize;
	private String screenSize;	
	
	List<ExtraField> listExtraFields;
	List<QuestionAnswer> questionAnswers;
	//*************
	private String resumeUrl;
	private String coverletterUrl;
	
	//############################################################################################################
    public String getAdvertisementId() {
        return advertisementId;
    }

    public void setAdvertisementId(String advertisementId) {
        this.advertisementId = advertisementId;
    }

    public List<String> getSkillGroupsList() {
        return skillGroupsList;
    }

    public void setSkillGroupsList(List<String> skillGroupsList) {
        this.skillGroupsList = skillGroupsList;
    }

    public String getVacancyId() {
        return vacancyId;
    }

    public void setVacancyId(String vacancyId) {
        this.vacancyId = vacancyId;
    }


    public String getSelectionCriteriaName() {
		return selectionCriteriaName;
	}

	public void setSelectionCriteriaName(String selectionCriteriaName) {
		this.selectionCriteriaName = selectionCriteriaName;
	}
	
	public String getSelectionCriteriaOriginalName() {
		return selectionCriteriaOriginalName;
	}

	public void setSelectionCriteriaOriginalName(String selectionCriteriaOriginalName) {
		this.selectionCriteriaOriginalName = selectionCriteriaOriginalName;
	}

	public String getSelectionCriteriaUrl() {
		return selectionCriteriaUrl;
	}

	public List<ExtraField> getListExtraFields() {
		return listExtraFields;
	}

	public void setListExtraFields(List<ExtraField> listExtraFields) {
		this.listExtraFields = listExtraFields;
	}

	public void setSelectionCriteriaUrl(String selectionCriteriaUrl) {
		this.selectionCriteriaUrl = selectionCriteriaUrl;
	}
	public String getScreenUrl() {
		return screenUrl;
	}
	public void setScreenUrl(String screenUrl) {
		this.screenUrl = screenUrl;
	}
	public String getSelectionCriteriaSize() {
		return selectionCriteriaSize;
	}
	public void setSelectionCriteriaSize(String selectionCriteriaSize) {
		this.selectionCriteriaSize = selectionCriteriaSize;
	}
	public String getScreenSize() {
		return screenSize;
	}
	public void setScreenSize(String screenSize) {
		this.screenSize = screenSize;
	}
	public String getResumeSize() {
		return resumeSize;
	}
	public void setResumeSize(String resumeSize) {
		this.resumeSize = resumeSize;
	}
	public String getResumePath() {
		return resumePath;
	}
	public void setResumePath(String resumePath) {
		this.resumePath = resumePath;
	}
	public String getCoverletterSize() {
		return coverletterSize;
	}
	public void setCoverletterSize(String coverletterSize) {
		this.coverletterSize = coverletterSize;
	}
	public String getResumeContent() {
		return resumeContent;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public void setResumeContent(String resumeContent) {
		this.resumeContent = resumeContent;
	}
	public String getCoverletterName() {
		return coverletterName;
	}
	public void setCoverletterName(String coverletterName) {
		this.coverletterName = coverletterName;
	}
	
	public String getCandidateId() {
		return candidateId;
	}
	public void setCandidateId(String candidateId) {
		this.candidateId = candidateId;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getJobApplicationId() {
		return jobApplicationId;
	}
	public void setJobApplicationId(String jobApplicationId) {
		this.jobApplicationId = jobApplicationId;
	}
	public String getJobseekerId() {
		return jobseekerId;
	}
	public void setJobseekerId(String jobseekerId) {
		this.jobseekerId = jobseekerId;
	}
	public String getJobreference() {
		return jobreference;
	}
	public void setJobreference(String jobreference) {
		this.jobreference = jobreference;
	}
	public String getDateSubmitted() {
		return dateSubmitted;
	}
	public void setDateSubmitted(String dateSubmitted) {
		this.dateSubmitted = dateSubmitted;
	}
	public String getResumeName() {
		return resumeName;
	}
	public void setResumeName(String resumeName) {
		this.resumeName = resumeName;
	}
	
	public String getResumeOriginalName() {
		return resumeOriginalName;
	}

	public void setResumeOriginalName(String resumeOriginalName) {
		this.resumeOriginalName = resumeOriginalName;
	}

	public String getCoverletterOriginalName() {
		return coverletterOriginalName;
	}

	public void setCoverletterOriginalName(String coverletterOriginalName) {
		this.coverletterOriginalName = coverletterOriginalName;
	}

	public String getResumeUrl() {
		return resumeUrl;
	}
	public void setResumeUrl(String resumeUrl) {
		this.resumeUrl = resumeUrl;
	}
	public String getCoverletterUrl() {
		return coverletterUrl;
	}
	public void setCoverletterUrl(String coverletterUrl) {
		this.coverletterUrl = coverletterUrl;
	}
	
	public String getOrgUsername() {
		return orgUsername;
	}
	public void setOrgUsername(String orgUsername) {
		this.orgUsername = orgUsername;
	}
	public List<QuestionAnswer> getQuestionAnswers() {
		return questionAnswers;
	}

	public void setQuestionAnswers(List<QuestionAnswer> questionAnswers) {
		this.questionAnswers = questionAnswers;
	}

	@Override
	public String toString() {
		String resumeContent2 = "##null##";
		if(resumeContent != null) {
			if(resumeContent.trim().length() > 10) {
				resumeContent2 = resumeContent.trim().substring(0, 10) + "##...##";
			} else {
				resumeContent2 = resumeContent;
			}			
		}
		return "Candidate [orgUsername=" + orgUsername + ", id=" + id
				+ ", firstname=" + firstname + ", lastname=" + lastname
				+ ", email=" + email + ", phone=" + phone
				+ ", jobApplicationId=" + jobApplicationId + ", jobseekerId="
				+ jobseekerId + ", jobreference=" + jobreference
				+ ", dateSubmitted=" + dateSubmitted + ", candidateId="
				+ candidateId + ", resumeName=" + resumeName
				+ ", coverletterName=" + coverletterName + ", resumeContent="
				+ resumeContent2 + ", resumeSize=" + resumeSize
				+ ", coverletterSize=" + coverletterSize
				+ ", selectionCriteriaUrl=" + selectionCriteriaUrl
				+ ", screenUrl=" + screenUrl + ", selectionCriteriaName="
				+ selectionCriteriaName + ", selectionCriteriaSize="
				+ selectionCriteriaSize + ", screenSize=" + screenSize
				+ ", !!!!!!listExtraFields=" + listExtraFields + "!!!!!, resumeUrl="
				+ resumeUrl + ", coverletterUrl=" + coverletterUrl + "]";
	}
}
