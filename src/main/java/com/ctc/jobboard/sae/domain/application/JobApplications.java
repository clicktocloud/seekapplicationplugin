package com.ctc.jobboard.sae.domain.application;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "JobApplications", propOrder = {
		"afterDate",
		"advertiserId",
    "jobApplication"
})
public class JobApplications {
	@XmlAttribute(name="afterDate" )
	private String afterDate;
	@XmlAttribute(name="advertiserId")
	private Integer advertiserId;

	@XmlElement(name="jobApplication")
	private JobApplication[] jobApplication;

	public String getAfterDate() {
		return afterDate;
	}

	public void setAfterDate(String afterDate) {
		this.afterDate = afterDate;
	}

	public Integer getAdvertiserId() {
		return advertiserId;
	}

	public void setAdvertiserId(Integer advertiserId) {
		this.advertiserId = advertiserId;
	}

	public JobApplication[] getJobApplication() {
		return jobApplication;
	}

	public void setJobApplication(JobApplication[] jobApplication) {
		this.jobApplication = jobApplication;
	}

	@Override
	public String toString() {
		return "ClassPojo [afterDate = " + afterDate + ", advertiserId = "
				+ advertiserId + ", jobApplication = " + jobApplication + "]";
	}
}
