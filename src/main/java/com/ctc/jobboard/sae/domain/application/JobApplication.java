package com.ctc.jobboard.sae.domain.application;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "JobApplication", propOrder = {
		"id",
		"firstName",
		"lastName",
		"phone",
		"jobReference",
		"jobSeekerId",
		"email",
		"dateSubmitted",
		"attachment"
})
public class JobApplication {
	@XmlAttribute(name="id")
	private String id;

	@XmlAttribute(name="firstName")
	private String firstName;
	
	@XmlAttribute(name="lastName")
	private String lastName;

	@XmlAttribute(name="phone")
	private String phone;

	@XmlAttribute(name="jobReference")
	private String jobReference;

	@XmlAttribute(name="jobSeekerId")
	private String jobSeekerId;

	@XmlAttribute(name="email")
	private String email;

	@XmlAttribute(name="dateSubmitted")
	private String dateSubmitted;

	@XmlElement(name="attachment")
	private Attachment[] attachment;

	

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getJobReference() {
		return jobReference;
	}

	public void setJobReference(String jobReference) {
		this.jobReference = jobReference;
	}

	public String getJobSeekerId() {
		return jobSeekerId;
	}

	public void setJobSeekerId(String jobSeekerId) {
		this.jobSeekerId = jobSeekerId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDateSubmitted() {
		return dateSubmitted;
	}

	public void setDateSubmitted(String dateSubmitted) {
		this.dateSubmitted = dateSubmitted;
	}

	public Attachment[] getAttachment() {
		return attachment;
	}

	public void setAttachment(Attachment[] attachment) {
		this.attachment = attachment;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@Override
	public String toString() {
		return "ClassPojo [id = " + id + ", lastName = " + lastName
				+ ", phone = " + phone + ", jobReference = " + jobReference
				+ ", jobSeekerId = " + jobSeekerId + ", email = " + email
				+ ", dateSubmitted = " + dateSubmitted + ", attachment = "
				+ attachment + ", firstName = " + firstName + "]";
	}
}
