package com.ctc.jobboard.sae.domain.application;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Attachment", propOrder = {
		"hash",
		"type",
		"dateUploaded",
		"href",
		"size"
})

public class Attachment {
	@XmlAttribute(name="hash")
	private String hash;

	@XmlAttribute(name="type")
	private String type;

	@XmlAttribute(name="dateUploaded")
	private String dateUploaded;

	@XmlAttribute(name="href")
	private String href;

	@XmlAttribute(name="size")
	private Integer size;

	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDateUploaded() {
		return dateUploaded;
	}

	public void setDateUploaded(String dateUploaded) {
		this.dateUploaded = dateUploaded;
	}

	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	public Integer getSize() {
		return size;
	}

	public void setSize(Integer size) {
		this.size = size;
	}

	@Override
	public String toString() {
		return "ClassPojo [hash = " + hash + ", type = " + type
				+ ", dateUploaded = " + dateUploaded + ", href = " + href
				+ ", size = " + size + "]";
	}
}
