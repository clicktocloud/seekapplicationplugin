package com.ctc.jobboard.sae.domain.screen;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "question", propOrder = {
    "id",
    "text",
    "type",
    "answer"
    
})
public class Question{
	@XmlAttribute(name = "id")
    private String id;

	@XmlAttribute(name = "text")
    private String text;
    
	@XmlAttribute(name = "type")
    private String type;

    @XmlElement(name="answer")
    private Answer[] answer;

   

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getText ()
    {
        return text;
    }

    public void setText (String text)
    {
        this.text = text;
    }

    public Answer[] getAnswer ()
    {
        return answer;
    }

    public void setAnswer (Answer[] answer)
    {
        this.answer = answer;
    }

    public String getType ()
    {
        return type;
    }

    public void setType (String type)
    {
        this.type = type;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [id = "+id+", text = "+text+", answer = "+answer+", type = "+type+"]";
    }
}