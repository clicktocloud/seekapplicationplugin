package com.ctc.jobboard.sae.domain.screen;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.ctc.jobboard.sae.domain.SAEBaseResponse;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "screen", propOrder = {
    "id",
    "name",
    "successful",
    "question"
    
})

@XmlRootElement( name = "screen")
public class SAEScreenResponse extends SAEBaseResponse {

	@XmlAttribute(name = "id")
	private String id;

	@XmlAttribute(name = "name")
    private String name;

	@XmlAttribute(name = "successful")
    private String successful;

	@XmlElement(name = "question")
    private Question[] question;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getSuccessful ()
    {
        return successful;
    }

    public void setSuccessful (String successful)
    {
        this.successful = successful;
    }

    public Question[] getQuestion ()
    {
        return question;
    }

    public void setQuestion (Question[] question)
    {
        this.question = question;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [id = "+id+", name = "+name+", successful = "+successful+", question = "+question+"]";
    }
}
