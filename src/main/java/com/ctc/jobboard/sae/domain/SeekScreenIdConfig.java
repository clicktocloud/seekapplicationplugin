package com.ctc.jobboard.sae.domain;

public class SeekScreenIdConfig {
	public SeekScreenIdConfig() {
		super();
	}
	public SeekScreenIdConfig(String screenId, String apiname, String questionId) {
		super();
		this.screenId = screenId;
		this.apiname = apiname;
		this.questionId = questionId;
	}

	private String screenId;
	private String apiname;	
	public String getScreenId() {
		return screenId;
	}
	public void setScreenId(String screenId) {
		this.screenId = screenId;
	}
	public String getQuestionId() {
		return questionId;
	}
	public void setQuestionId(String questionId) {
		this.questionId = questionId;
	}

	private String questionId;
	
	public String getApiname() {
		return apiname;
	}
	public void setApiname(String apiname) {
		this.apiname = apiname;
	}
	@Override
	public String toString() {
		return "SeekScreenIdConfig [screenId=" + screenId + ", apiname="
				+ apiname + ", questionId=" + questionId + "]";
	}
}