package com.ctc.jobboard.sae.domain;

import com.ctc.jobboard.core.application.QuestionAnswerType;

public class QuestionAnswer implements QuestionAnswerType {
	
	private String apiname;	
	private String screenId;
	private String questionId;
	private String answer;
	private Object type;
	
	
	public QuestionAnswer(String screenId, String questionId, String answer) {
		super();
		this.questionId = questionId;
		this.answer = answer;
	}
	
	
	public QuestionAnswer(String apiname,String screenId, String questionId, String answer) {
		super();
		this.apiname = apiname;
		this.screenId = screenId;
		this.questionId = questionId;
		this.answer = answer;
	}


	public String getApiname() {
		return apiname;
	}
	public void setApiname(String apiname) {
		this.apiname = apiname;
	}
	
	public String getAnswer() {
		return answer;
	}
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	public Object getType() {
		return type;
	}
	public void setType(Object type) {
		this.type = type;
	}


	public String getScreenId() {
		return screenId;
	}


	public void setScreenId(String screenId) {
		this.screenId = screenId;
	}


	public String getQuestionId() {
		return questionId;
	}


	public void setQuestionId(String questionId) {
		this.questionId = questionId;
	}

}
