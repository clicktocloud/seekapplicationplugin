package com.ctc.jobboard.sae.domain;

import java.util.HashMap;
import java.util.Map;


public class ExtraField {
	
	public static Map<Object,Class<?>> typesMap = new HashMap<Object,Class<?>> ();
	static{
		//TODO init typesMap
		
	}
	private String apiname;	
	private String value;
	private Object type;
	public String getApiname() {
		return apiname;
	}
	public void setApiname(String apiname) {
		this.apiname = apiname;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	@Override
	public String toString() {
		String value2 = "##null##";
		if(value != null) {
			if(value.trim().length() > 10) {
				value2 = value.trim().substring(0, 10) + "##...##";
			} else {
				value2 = value;
			}
		}
		return "ExtraField [apiname=" + apiname + ", value=" + value2 + "]";
	}
	public ExtraField(String apiname, String value) {
		super();
		this.apiname = apiname;
		this.value = value;
	}
	public ExtraField() {
		super();
	}
	public void setType(Object fieldType) {
		this.type = fieldType;
		
	}
	public Object getType() {
		return type;
	}
	
	public Object getConvertedValue(){
		Object converted = null;
		if(getType() != null && typesMap.get(getType()).equals(Boolean.class)){
    		if(getValue() != null && (getValue().toLowerCase().contains("true") || getValue().toLowerCase().contains("yes"))){
    			converted = true;
    		}else{
    			converted = false;
    		}
    	}else{
    		converted = getValue();
    	}
		
		return converted;
	}
}
