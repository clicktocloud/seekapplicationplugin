package com.ctc.jobboard.sae.domain.oauth;

import com.ctc.jobboard.sae.domain.SAEBaseResponse;

public class SAEOAuthResponse extends SAEBaseResponse{
	
	public String getAccessToken(){
		return getHeader("X-SeekAPI-Token");
	}

	

}