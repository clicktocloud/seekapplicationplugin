package com.ctc.jobboard.sae.domain.application;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.ctc.jobboard.sae.domain.SAEBaseResponse;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "seek", propOrder = {
    "jobApplications"
})

@XmlRootElement( name = "seek")
public class SAEApplicationResponse extends SAEBaseResponse {
	@XmlElement(name="jobApplications")
	private JobApplications jobApplications;

    public JobApplications getJobApplications ()
    {
        return jobApplications;
    }

    public void setJobApplications (JobApplications jobApplications)
    {
        this.jobApplications = jobApplications;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [jobApplications = "+jobApplications+"]";
    }

}
