package com.ctc.jobboard.sae.domain.screen;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;




@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "answer", propOrder = {
    "id",
    "content",
    "selected"
})
public class Answer
{
	@XmlAttribute(name="id")
    private String id;

	@XmlValue
    private String content;
    
    @XmlAttribute(name="selected")
    private String selected;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getContent ()
    {
        return content;
    }

    public void setContent (String content)
    {
        this.content = content;
    }

    public String getSelected() {
		return selected;
	}

	public void setSelected(String selected) {
		this.selected = selected;
	}

	@Override
    public String toString()
    {
        return "ClassPojo [id = "+id+", content = "+content+"]";
    }
}