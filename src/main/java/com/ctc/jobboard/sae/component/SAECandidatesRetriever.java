package com.ctc.jobboard.sae.component;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.ctc.jobboard.core.JobBoard;
import com.ctc.jobboard.core.JobBoardRequestPackage;
import com.ctc.jobboard.core.application.ApplicationRetriever;
import com.ctc.jobboard.core.application.CandidateType;
import com.ctc.jobboard.sae.domain.Candidate;
import com.ctc.jobboard.sae.domain.application.SAEApplicationResponse;

public class SAECandidatesRetriever extends ApplicationRetriever<CandidateType> {
	
	static Logger logger = Logger.getLogger(SAECandidatesRetriever.class);

	public SAECandidatesRetriever(JobBoard jb) {
		super(jb);	
	}
	{
		addSubRetriever(new SAEScreenQuestionsRetriever(jb));
		addSubRetriever(new SAECoverLettersandSelectionCriteriaRetriever(jb));
		
	}
	
	public SAEJobBoard getJobBoard(){
		return (SAEJobBoard) jb;
	}
	
	@Override
	public List<Candidate> retrieve() {
		
		List<Candidate> candidates = fetchCandidates();
		
		logger.debug("Total candidates : " + (candidates == null ? 0 : candidates.size()));
		
		for(ApplicationRetriever<?> sub : getSubRetrievers()){
			sub.setCandidateTypes(candidates);
			sub.retrieve();
		}
		
		return candidates;
	}
	
	public List<Candidate> fetchCandidates(){
		
		List<Candidate> candidates  = new ArrayList<Candidate>();
		
		SAEAccount account = (SAEAccount) jb.getAccount();
		
		boolean done = false;
		
		String afterId = account.getOrgMaxContactid();
		int firstBatchAmount  = -1 ;
		while( ! done ){
			List<Candidate> batch = fetchOneBatchCandidates(account.getAccountId(), afterId );
			
			logger.debug("afterId : " + afterId);
			if(batch != null && batch.size() > 0 ){
				if( firstBatchAmount < 0 ){
					firstBatchAmount = batch.size();
					//logger.debug("first batch amount : " +firstBatchAmount );
				}
				candidates.addAll( batch );
				
				String lastId = batch.get( batch.size() - 1 ).getJobApplicationId();
				if(StringUtils.isEmpty( lastId) ){
					done = true;
				}else{
					if( batch.size() < firstBatchAmount){
						done = true;
					}else{
						afterId = lastId;
					}	
				}
			}else{
				done = true;
			}
	
		}
		
		return candidates;
		
	}
	public List<Candidate> fetchOneBatchCandidates(String advertiserId, String afterId){
		
		String url = "";
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);    
        String yesterdayDate = dateFormat.format(cal.getTime());
		if (afterId == null || "1".equals(afterId)  ){
			url = SAEJobBoard.SERVICE_DOMAIN + "/advertisers/" + advertiserId + "/jobApplications?AfterDate="+yesterdayDate;			
		} else {
			url = SAEJobBoard.SERVICE_DOMAIN + "/advertisers/" + advertiserId + "/jobApplications?afterid=" + afterId;
		}	
		
		
		JobBoardRequestPackage requestPackage = getJobBoard().buildRequestPackage( );
		
		requestPackage.setUrl(url);
		
		getJobBoard().setActionType(SAEJobBoard.FETCH);
		
		SAEApplicationResponse response = getJobBoard().execute(requestPackage, SAEApplicationResponse.class);
		
		getJobBoard().setResponsePackage(new SAEApplicationsResponsePackage(response));
		getJobBoard().setResponseHandler(new SAEApplicationsResponseHandler(getJobBoard()));
		getJobBoard().responseHandler();
		
		SAEApplicationsResponseHandler responseHandler = (SAEApplicationsResponseHandler) getJobBoard().getResponseHandler();
		
		return responseHandler.getResult();
			
		
	}

}
