package com.ctc.jobboard.sae.component;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.log4j.Logger;

import com.ctc.jobboard.core.JobBoard;
import com.ctc.jobboard.core.JobBoardRequest;
import com.ctc.jobboard.core.JobBoardRequestPackage;
import com.ctc.jobboard.core.JobBoardResponse;
import com.ctc.jobboard.core.application.ApplicationJobBoard;
import com.ctc.jobboard.core.application.ApplicationRetriever;
import com.ctc.jobboard.core.application.CandidateType;
import com.ctc.jobboard.sae.domain.Candidate;
import com.ctc.jobboard.sae.domain.DownloadedFile;
import com.ctc.jobboard.sae.domain.SAEDownloadFileResponse;
import com.ctc.jobboard.sae.utils.SAEConfig;

public class SAECoverLettersandSelectionCriteriaRetriever extends ApplicationRetriever<DownloadedFile> {
	
	static Logger logger = Logger.getLogger(SAECoverLettersandSelectionCriteriaRetriever.class);
	

	public SAECoverLettersandSelectionCriteriaRetriever(JobBoard jb) {
		super(jb);
		
	}
	
	public List<DownloadedFile> retrieve(){
		
		return fetchCoverLettersandSelectionCriteria();
		
	}
	
	public SAEJobBoard getJobBoard(){
		return (SAEJobBoard) jb;
	}
	
	public List<DownloadedFile> fetchCoverLettersandSelectionCriteria( ){
		if(candidateTypes == null)
			return null;
		
		List<DownloadedFile> dfs = new ArrayList<DownloadedFile>();
		
		getJobBoard().setActionType(SAEJobBoard.FETCH);
		
		logger.debug("Begin to download files");
		for(CandidateType ct : candidateTypes){
			
			Candidate candidate = (Candidate) ct;
			try {
				// Download resume
				if(StringUtils.isNotBlank(candidate.getResumeUrl())){
					
					DownloadedFile df= downloadFile(candidate.getResumeUrl(),getJobBoard().getOperator(), getJobBoard().getBucket());
					candidate.setResumeOriginalName(df.getOrignalFilename());
					candidate.setResumeName(df.getNewFilename());
								
					// save the resume path for daxtra parsing later
					candidate.setResumePath(df.getFile().getAbsolutePath());	
					
					logger.debug("\tDownloaded "+candidate.getJobApplicationId() + "'s file : "+df.getNewFilename());
					
					dfs.add(df);
				}else{
                    logger.warn("missing resume ...");
                }
				
				// Download cover letter
				if(StringUtils.isNotBlank(candidate.getCoverletterUrl())){
					DownloadedFile df= downloadFile(candidate.getCoverletterUrl(),getJobBoard().getOperator(), getJobBoard().getBucket());
					
					candidate.setCoverletterOriginalName(df.getOrignalFilename());
					candidate.setCoverletterName(df.getNewFilename());
					
					logger.debug("\tDownloaded "+candidate.getJobApplicationId() + "'s file : "+df.getNewFilename());
					
					dfs.add(df);
				}
				
				// Download selection criteria
				if(StringUtils.isNotBlank(candidate.getSelectionCriteriaUrl())){
					DownloadedFile df= downloadFile(candidate.getSelectionCriteriaUrl(),getJobBoard().getOperator(), getJobBoard().getBucket());
					
					candidate.setSelectionCriteriaOriginalName(df.getOrignalFilename());
					candidate.setSelectionCriteriaName(df.getNewFilename());
					
					logger.debug("\tDownloaded "+candidate.getJobApplicationId() + "'s file : "+df.getNewFilename());
					
					dfs.add(df);
				}		
				
				
				
				
			}catch(Exception e){
				logger.error("cannot download resume or cover letter or selection criteria for candidate "+candidate);
				logger.error(e);
			}
		}
		logger.info("end of downloading documents ");
		
		return dfs;
	}
	
	
	private DownloadedFile downloadFile( String url,String username, String bucket){
		DownloadedFile df = new DownloadedFile() ;
		
		getJobBoard().setActionType(SAEJobBoard.FETCH);
		
		JobBoardRequestPackage requestPackage = getJobBoard().buildRequestPackage( );
		requestPackage.setUrl(SAEJobBoard.SERVICE_DOMAIN +""+url );
		requestPackage.addHeader(JobBoardRequest.ENTITY_HEADER, "any");

		SAEDownloadFileResponse response = getJobBoard().execute(requestPackage, SAEDownloadFileResponse.class);
		
		String fileName = getFileName(response);
		
		String newFileName = ApplicationJobBoard.changeFileName(fileName);
		
		String tempfolder = SAEConfig.getTempFolder(username,bucket);
		File folder = new File(tempfolder);
		if(! folder.exists()){
			folder.mkdir();
		}
		
		File file = new File(SAEConfig.getTempFolder(username,bucket)+newFileName);
		
		HttpEntity entity =(HttpEntity) response.getHttpEntity();
		InputStream is = null;
		OutputStream os = null;
		
		try {
			is = entity.getContent();
			os = new FileOutputStream(SAEConfig.getTempFolder(username,bucket)+newFileName);
			IOUtils.copy(is, os);
			os.close();
			
			df.setOrignalFilename(fileName);
			df.setNewFilename(newFileName);
			df.setFile(file);
			
			logger.debug("Downloaded - " + file.getAbsolutePath());
			
		} catch (Exception e) {
			logger.error(e);
		} finally{
			if(is != null){
				try {
					is.close();
				} catch (IOException e) {
					
				} 
			}
			if(os != null){
				try {
					os.close();
				} catch (IOException e) {
					
				} 
			}
		}
		
		
		return df;
		
	}
	
	private String getFileName(JobBoardResponse response){
		String disposition = response.getHeader("Content-Disposition");
		int index = disposition.lastIndexOf("filename=");
		String fileName = disposition.substring(index+9);
		
		return fileName;
	}

}
