package com.ctc.jobboard.sae.component;

import java.util.ArrayList;
import java.util.List;

import com.ctc.jobboard.core.BasicResponsePackage;
import com.ctc.jobboard.core.JobBoard;
import com.ctc.jobboard.core.ResponseHandler;
import com.ctc.jobboard.core.application.ApplicationResponsePackage;
import com.ctc.jobboard.core.application.ApplicationResponsePackage.Application;
import com.ctc.jobboard.core.application.ApplicationResponsePackage.Attachment;
import com.ctc.jobboard.persistence.JBConnection;
import com.ctc.jobboard.sae.domain.Candidate;

public class SAEApplicationsResponseHandler implements ResponseHandler<Candidate> {
	
	private SAEJobBoard jobBoard;
	private List<Candidate> candidates = new ArrayList<Candidate>();
	
	
	public SAEApplicationsResponseHandler( SAEJobBoard jobBoard){
		this.jobBoard = jobBoard;
		
	}

	@Override
	public  void handle(JBConnection jbConnection, BasicResponsePackage responsePackage) {
		
		if(responsePackage  == null ){
			return;
		}
		ApplicationResponsePackage appPackage = (ApplicationResponsePackage)  responsePackage ;

		if(appPackage.getAllApplications()  == null ){
			return;
		}
		
		for( Application app : appPackage.getAllApplications()){
			
			if( isInValidJobReference( app.getJobReference())){
				continue;
			}
			
			
			Candidate candidate = new Candidate();
			
			candidate.setOrgUsername		(jobBoard.getOperator());
			candidate.setFirstname			(app.getFirstName());
			candidate.setLastname			(app.getLastName());
			candidate.setDateSubmitted		(app.getDateSubmitted());
			candidate.setEmail				(app.getEmail());
			candidate.setPhone				(app.getPhone());
			candidate.setJobseekerId		(app.getJobSeekerId());
			candidate.setJobApplicationId	(app.getId());		
			candidate.setResumeContent		("The document parsing is now in progress. Please check again in a couple of minutes.");
			
			
			candidate.setJobreference( fixJobReference(app.getJobReference()));
			
			
			if(app.getAttachments() != null ){
				for(Attachment att : app.getAttachments()){					
					
					if(att.getType().toLowerCase().equals("resume")){
						candidate.setResumeUrl(att.getHref());
						candidate.setResumeSize(""+ att.getSize());
						
					}else if(att.getType().toLowerCase().equals	("coverletter")){
						candidate.setCoverletterUrl(att.getHref());
						candidate.setCoverletterSize		(""+ att.getSize());
						
					}else if(att.getType().toLowerCase().equals("selectioncriteria")){
						candidate.setSelectionCriteriaUrl	(att.getHref());
						candidate.setSelectionCriteriaSize	(""+ att.getSize());
						
					}else if(att.getType().toLowerCase().equals("screen")){
						candidate.setScreenUrl				(att.getHref());
						candidate.setScreenSize				(""+ att.getSize());
					}else{
						// do nothing.
					}
				}
			}
			
			candidates.add(candidate);
			
		}
	}

	@Override
	public JobBoard getJobBoard() {
		
		return jobBoard;
	}

	@Override
	public List<Candidate> getResult() {
		return candidates;
	}

	@Override
	public void setJobBoard(JobBoard jobboard) {
		this.jobBoard =(SAEJobBoard) jobboard;
	}
	
	public String fixJobReference( String rawReference){
		String fixed = "";
		int jobRefWith18DigitAdIdLength = 34;
		if(isPremiunListing(rawReference)){	// check if premium listing is being used
			if(rawReference.length() >= (jobRefWith18DigitAdIdLength + 3)){
				fixed = rawReference.substring(0, jobRefWith18DigitAdIdLength);	// 18 digit 	
			}else{
				fixed = rawReference.substring(0, jobRefWith18DigitAdIdLength-3); 	// 15 digit
			}
		}else{
			if(rawReference.length() >= jobRefWith18DigitAdIdLength ){
				fixed = rawReference.substring(0, jobRefWith18DigitAdIdLength); 	// 18 digit
			}else{
				fixed = rawReference.substring(0, jobRefWith18DigitAdIdLength-3); 	// 15 digit
			}
		}
		
		return fixed;
	}
	
	public static boolean isPremiunListing(String jobReference){
		if(jobReference.indexOf(".") != -1 || (jobReference.length() > 34 && jobReference.indexOf("PL") != -1)){
			return true;
		}else{
			return false;
		}
	}
	
	public static boolean isInValidJobReference(String jobReference){
		return jobReference.indexOf(":")==-1 || jobReference.split(":")[0].length()!=15;
			
	}

}
