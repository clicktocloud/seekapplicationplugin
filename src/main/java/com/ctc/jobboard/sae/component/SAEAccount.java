package com.ctc.jobboard.sae.component;

import java.util.List;

import com.ctc.jobboard.component.BasicAccount;
import com.ctc.jobboard.sae.domain.SeekScreenIdConfig;
public class SAEAccount extends BasicAccount {
	
	private List<SeekScreenIdConfig> listSeekScreenIdConfig;
	private String orgMaxContactid;
	private String orgUsername;
	private String bucket;
	private String orgId;
   
	public SAEAccount(List<SeekScreenIdConfig> listSeekScreenIdConfig,String orgMaxContactid, String accountId) {
		super(accountId,  "",  accountId);
		this.listSeekScreenIdConfig = listSeekScreenIdConfig;
		this.orgMaxContactid = orgMaxContactid;
		
	}
	public SAEAccount(String username, String password, String accountId){
		super( username,  password,  accountId);
		
		
	}
	public List<SeekScreenIdConfig> getListSeekScreenIdConfig() {
		return listSeekScreenIdConfig;
	}
	public void setListSeekScreenIdConfig(
			List<SeekScreenIdConfig> listSeekScreenIdConfig) {
		this.listSeekScreenIdConfig = listSeekScreenIdConfig;
	}
	public String getOrgMaxContactid() {
		return orgMaxContactid;
	}
	public void setOrgMaxContactid(String orgMaxContactid) {
		this.orgMaxContactid = orgMaxContactid;
	}
	public String getAccountId() {
		return getAdvertiserId();
	}
	public void setAccountId(String accountId) {
		setAdvertiserId(accountId);
	}
	public String getOrgUsername() {
		return orgUsername;
	}
	public void setOrgUsername(String orgUsername) {
		this.orgUsername = orgUsername;
	}
	public String getBucket() {
		return bucket;
	}
	public void setBucket(String bucket) {
		this.bucket = bucket;
	}
	public void setOrgId(String id15) {
		// TODO Auto-generated method stub
		
	}
	public String getOrgId() {
		return orgId;
	}
	
	
	
	

}
