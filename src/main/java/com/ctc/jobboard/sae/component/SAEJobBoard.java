package com.ctc.jobboard.sae.component;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.ctc.jobboard.core.JobBoard;
import com.ctc.jobboard.core.JobBoardRequestPackage;
import com.ctc.jobboard.core.JobBoardRequestPackage.MethodType;
import com.ctc.jobboard.core.JobBoardResponse;
import com.ctc.jobboard.core.JobContentXmlConverter;
import com.ctc.jobboard.core.application.ApplicationJobBoard;
import com.ctc.jobboard.exception.AccountException;
import com.ctc.jobboard.exception.JobBoardException;
import com.ctc.jobboard.persistence.JBConnectionManager;
import com.ctc.jobboard.sae.domain.Candidate;
import com.ctc.jobboard.sae.domain.ExtraField;
import com.ctc.jobboard.sae.domain.SAEBaseRequest;
import com.ctc.jobboard.sae.domain.SAEDownloadFileResponse;
import com.ctc.jobboard.sae.domain.SeekScreenIdConfig;
import com.ctc.jobboard.sae.domain.application.SAEApplicationResponse;
import com.ctc.jobboard.sae.domain.screen.SAEScreenResponse;
import com.ctc.jobboard.sae.utils.SAEConfig;
import com.ctc.jobboard.util.BasicConfig;

public  class SAEJobBoard extends ApplicationJobBoard{
	
	
	
	public static final String FETCH="FETCH" ;
	
	public static final String DEFAULT_BOARD_NAME ;
	public static final String DEFAULT_BOARD_API_NAME ;
	
	
	
	public static final String ACCEPT = "application/xml";
	public static final String CONTENT_TYPE_URLENCODED = "application/x-www-form-urlencoded";
	
	public static final String CONTENT_TYPE_XML= "application/xml";
	
	public static final String SERVICE_DOMAIN ;
	
	
	static{
		
		SERVICE_DOMAIN = StringUtils.isEmpty( BasicConfig.get("sae_endpoint")) ? "https://test.api.seek.com.au" : BasicConfig.get("sae_endpoint");
		DEFAULT_BOARD_NAME = StringUtils.isEmpty( BasicConfig.get("sae_default_jobboard_name")) ? "SEEK" : BasicConfig.get("sae_default_jobboard_name");
		DEFAULT_BOARD_API_NAME = StringUtils.isEmpty( BasicConfig.get("sae_default_jobboard_apiname")) ? "SEEK__c" : BasicConfig.get("sae_default_jobboard_apiname");
	
	}
	
	
	
	
	static Logger logger = Logger.getLogger(SAEJobBoard.class);
	
	private String operator;
	private List<SeekScreenIdConfig> listSeekScreenIdConfig;
	private String bucket;
	
	
	public SAEJobBoard(String jobBoardName){
		super(jobBoardName, new JobContentXmlConverter());
		
		setCandidateRetriever(new SAECandidatesRetriever(this));
	}
	
	

	public SAEAccount buildAccount(String advertiserId, String username, String password){
		return new SAEAccount(username, password, advertiserId);
	}
	
	{
		successCodes.add(200);
		
		
	}
	

	public String getAccessToken(){
		String accessToken = "";
		SAEAccount account = (SAEAccount) getAccount();
		if(account != null){
			String currentToken = account.getAccessToken();
			accessToken = currentToken;
			int expireIn = account.getAccessTokenExpireIn();
			Date from = account.getAccessTokenFrom();
			
			
			if(accessToken==null 
					|| accessToken.equals("") 
					|| from == null 
					|| ((from.getTime() + expireIn * 1000) - new Date().getTime()   <= SAEOAuth.BEFORE_EXPIRED_TIME) ){
				
				SAEOAuth oauth = new SAEOAuth();
				accessToken  = oauth.retrieveAccessToken(account);
					
			}			
		}
		return accessToken;
	}
	

	public JobBoardRequestPackage buildRequestPackage(){
		JobBoardRequestPackage requestPackage = new JobBoardRequestPackage();
		requestPackage.setRequest(new SAEBaseRequest());
		requestPackage.setMethodType(MethodType.GET);
		requestPackage.addHeader("Accept", 			SAEJobBoard.ACCEPT);
		requestPackage.addHeader("Host", 			SAEConfig.getSAEHost());
		
		return requestPackage;
		
	}
	

	
	public SAEApplicationResponse createSAEFetchApplicationResponse( String message, String code, String actionMessage, String referenceNo){
		//TODO

		return null;
	}
	

	protected <R extends JobBoardResponse> R execute(JobBoardRequestPackage requestPackage, Class<R> clazz) {
		
		requestPackage.addHeader("Authorization", getAccessToken());
		
		return super.execute(requestPackage, clazz);
			
	}
	
	protected void throwExceptionFromUnsuccessfulJobPostResponse(){
		
	}



	public String getOperator() {
		return operator;
	}



	public void setOperator(String operator) {
		this.operator = operator;
	}



	public List<SeekScreenIdConfig> getListSeekScreenIdConfig() {
		return listSeekScreenIdConfig;
	}



	public void setListSeekScreenIdConfig(
			List<SeekScreenIdConfig> listSeekScreenIdConfig) {
		this.listSeekScreenIdConfig = listSeekScreenIdConfig;
	}



	public String getBucket() {
		return bucket;
	}



	public void setBucket(String bucket) {
		this.bucket = bucket;
	}

	
}
