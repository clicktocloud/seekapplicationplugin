package com.ctc.jobboard.sae.component;

import com.ctc.jobboard.core.BasicResponsePackage;
import com.ctc.jobboard.sae.domain.screen.SAEScreenResponse;

public class SAEScreensResponsePackage implements BasicResponsePackage{
	
	private SAEScreenResponse response;

	public SAEScreensResponsePackage(
			SAEScreenResponse response) {
		this.response = response;
	}

	public SAEScreenResponse getResponse() {
		return response;
	}

}
