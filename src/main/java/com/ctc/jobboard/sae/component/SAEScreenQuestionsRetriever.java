package com.ctc.jobboard.sae.component;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.ctc.jobboard.core.JobBoard;
import com.ctc.jobboard.core.JobBoardRequestPackage;
import com.ctc.jobboard.core.application.ApplicationRetriever;
import com.ctc.jobboard.core.application.CandidateType;
import com.ctc.jobboard.sae.domain.Candidate;
import com.ctc.jobboard.sae.domain.ExtraField;
import com.ctc.jobboard.sae.domain.QuestionAnswer;
import com.ctc.jobboard.sae.domain.SeekScreenIdConfig;
import com.ctc.jobboard.sae.domain.screen.SAEScreenResponse;
import com.ctc.jobboard.sae.utils.SAEConfig;

public class SAEScreenQuestionsRetriever extends ApplicationRetriever<QuestionAnswer> {
	
	static Logger logger = Logger.getLogger(SAEScreenQuestionsRetriever.class);
	

	public SAEScreenQuestionsRetriever(JobBoard jb) {
		super(jb);
		
	}
	
	public List<QuestionAnswer> retrieve(){
		
		return fetchScreens();
		
	}
	
	public SAEJobBoard getJobBoard(){
		return (SAEJobBoard) jb;
	}
	
	public List<QuestionAnswer> fetchScreens( ){
		
		List<QuestionAnswer> allqas = new ArrayList<QuestionAnswer>();

		getJobBoard().setActionType(SAEJobBoard.FETCH);
		
		logger.debug("Begin to fetch screen questions");
		int index = 0;
		for(CandidateType ct : candidateTypes){
		
			Candidate candidate = (Candidate) ct;
			if(StringUtils.isNotBlank(candidate.getScreenUrl())){
				

				List<QuestionAnswer> qas = fetchScreenQuestionAnswers( candidate.getScreenUrl());
				
				logger.debug("\t["+ index++ +"] Candidate [" +candidate.getJobApplicationId() +"]'s screen questions : " + qas.size());;
				
				candidate.setListExtraFields( filterExtraFields( qas ));
				
				
				
				allqas.addAll(qas);
				
			}
			
		}
		logger.info("end of downloading questions & answers...");
		
		return allqas;
			
		
	}
	
	private List<QuestionAnswer> fetchScreenQuestionAnswers(  String screenUrl ){
		try{
			JobBoardRequestPackage requestPackage = getJobBoard().buildRequestPackage( );
			requestPackage.setUrl(SAEJobBoard.SERVICE_DOMAIN + ""+ screenUrl);
			
			SAEScreenResponse response = getJobBoard().execute(requestPackage, SAEScreenResponse.class);
			
			SAEScreensResponsePackage responsePackage = new SAEScreensResponsePackage(response);
			SAEScreensResponseHandler handler = new SAEScreensResponseHandler(getJobBoard());
			handler.handle(getJobBoard().getCurrentConnection(), responsePackage);

			return handler.getResult();
		}catch(Exception e){
			logger.error("cannot download ScreenUrl " + screenUrl);
		}
		
		return null;
		
	}
	
	private List<ExtraField> filterExtraFields(List<QuestionAnswer> qas ){
		
		if(qas == null)
			return null;
		
		List<ExtraField> extraFields = new ArrayList<ExtraField>();
		for(QuestionAnswer qa :qas){
			
			SeekScreenIdConfig seekScreenIdConfig = 
			getSeekScreenIdConfig(qa.getScreenId(), qa.getQuestionId(), getJobBoard().getListSeekScreenIdConfig());
			
			if (seekScreenIdConfig == null) {
				continue;
			}
			
			String apiName 	= seekScreenIdConfig.getApiname();
			
			String answer = qa.getAnswer();
			if(SAEConfig.ApiMaxLengthMap.containsKey(apiName)){
				int maxLength = SAEConfig.ApiMaxLengthMap.get(apiName);
				if(answer.length() > maxLength){
					logger.warn("ApiName:" + apiName + ", value:" + answer + " has been cut.");
					answer = answer.substring(0, maxLength);
				}	
			}
			
			extraFields.add(new ExtraField(apiName, answer));	
			
		}
		
		return extraFields;
		
	}
	
	private  SeekScreenIdConfig getSeekScreenIdConfig(String screenId, String questionId, List<SeekScreenIdConfig> listSeekScreenIdConfig) {
		SeekScreenIdConfig seekScreenIdConfig = null;
		if(listSeekScreenIdConfig != null){
			for (SeekScreenIdConfig s : listSeekScreenIdConfig) {
				if( s.getScreenId().equals(screenId) && s.getQuestionId().equals(questionId) ) {
					seekScreenIdConfig = s;
					break;
				}
			}
		}
		
		return seekScreenIdConfig;
	}

}
