package com.ctc.jobboard.sae.component;

import java.util.ArrayList;
import java.util.List;

import com.ctc.jobboard.core.application.ApplicationResponsePackage;
import com.ctc.jobboard.sae.domain.application.JobApplication;
import com.ctc.jobboard.sae.domain.application.SAEApplicationResponse;


public class SAEApplicationsResponsePackage implements ApplicationResponsePackage{
	

	
	private SAEApplicationResponse response;

	public SAEApplicationsResponsePackage(
			SAEApplicationResponse response) {
		this.response = response;
	}

	public SAEApplicationResponse getResponse() {
		return response;
	}
	
	
	public int getApplicationsAmount(){
		if( getResponse() == null 
				|| response.getJobApplications() == null
				|| response.getJobApplications().getJobApplication() == null)
			return 0;
		
		return response.getJobApplications().getJobApplication().length;
	}
	
	public List<String> getAllApplicationIds(){
		if( getResponse() == null 
				|| response.getJobApplications() == null
				|| response.getJobApplications().getJobApplication() == null)
			return null;
		
		List<String> allIds = new ArrayList<String>();
		for(JobApplication ja : response.getJobApplications().getJobApplication()){
			allIds.add(ja.getId());
		}
		
		return allIds;
	}
	
	public String getLastApplicationId(){
		
		List<String> allIds = getAllApplicationIds();
		if( allIds == null || allIds.size() == 0){
			return null;
		}
		
		return allIds.get( allIds.size() - 1 );
		
	}
	
	public List<Application> getAllApplications(){
		
		if( getResponse() == null 
				|| response.getJobApplications() == null
				|| response.getJobApplications().getJobApplication() == null)
			return null;
		
		List<Application> applications = new ArrayList<Application>();
		
		for(JobApplication ja : response.getJobApplications().getJobApplication()){
			Application app = new Application();
			applications.add(app);
			
			app.setId( ja.getId());
			app.setFirstName( ja.getFirstName());
			app.setLastName(ja.getLastName());
			app.setPhone(ja.getPhone());
			app.setEmail( ja.getEmail());
			app.setDateSubmitted(ja.getDateSubmitted());
			app.setJobSeekerId(ja.getJobSeekerId());
			app.setJobReference(ja.getJobReference());
			app.setAttachment(new ArrayList<Attachment>());
			
			
			if(ja.getAttachment() != null){
				
				for(com.ctc.jobboard.sae.domain.application.Attachment a : ja.getAttachment()){
					Attachment att = new Attachment();
					app.getAttachments().add(att);
					
					att.setHash( a.getHash());
					att.setType(a.getType());
					att.setHref(a.getHref());
					att.setSize(a.getSize());
					att.setDateUploaded(a.getDateUploaded());
					
				}
			}	
		}
		
		return applications;
		
	}
	

}
