package com.ctc.jobboard.sae.component;

import java.util.Date;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;

import com.ctc.jobboard.core.JobBoardRequestDispatcher;
import com.ctc.jobboard.core.JobBoardRequestPackage;
import com.ctc.jobboard.core.JobBoardRequestPackage.MethodType;
import com.ctc.jobboard.core.JobContentJsonConverter;
import com.ctc.jobboard.exception.BadRequestException;
import com.ctc.jobboard.sae.domain.oauth.SAEOAuthResponse;
import com.ctc.jobboard.sae.utils.SAEConfig;

public class SAEOAuth {
	public static final Integer BEFORE_EXPIRED_TIME = 1000; 
	public static final String TEST_CLIENT_ID = "0050569E4A781EE69BB16217DD7AE086";
	public static final String TEST_CLIENT_SECRET = "GAgfBwP8ywwFTwi6oGRRYTu5adsweKDC";
	public static final String TEST_ADVERTISER_ID = "34552313";
	private JobBoardRequestDispatcher service = new JobBoardRequestDispatcher();
	
	public String retrieveAccessToken(SAEAccount account){
		

		String token = "";
		
		JobBoardRequestPackage requestPackage = new JobBoardRequestPackage();
		requestPackage.setUrl(SAEJobBoard.SERVICE_DOMAIN + "/Authenticate/ThirdParty");
		requestPackage.setMethodType(MethodType.POST);
		
		requestPackage.addHeader("Host", SAEConfig.getSAEHost());
		
		String 	userpass 	= SAEConfig.getSAEUserName()+ ":" + SAEConfig.getSAEPassword();
		//String 	userpass 	= account.getUsername()+ ":" + account.getPassword();
		String 	authString 	= new String(Base64.encodeBase64(userpass.getBytes(), false));
		requestPackage.addHeader("Authorization", 	" Basic "+authString);
		
		try {
			SAEOAuthResponse response = service.execute(requestPackage, SAEOAuthResponse.class, new JobContentJsonConverter());
			
			token = response.getAccessToken();
			
			account.setAccessToken(token);
			account.setAccessTokenExpireIn( 24 * 60 * 60);
			account.setAccessTokenFrom( new Date());
			
		} catch (BadRequestException e) {
			
		}
		
		return token;
	}

}
