package com.ctc.jobboard.sae.component;

import java.util.ArrayList;
import java.util.List;

import javax.security.auth.login.Configuration;

import org.apache.log4j.Logger;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.ctc.jobboard.core.BasicResponsePackage;
import com.ctc.jobboard.core.JobBoard;
import com.ctc.jobboard.core.ResponseHandler;
import com.ctc.jobboard.persistence.JBConnection;
import com.ctc.jobboard.sae.domain.ExtraField;
import com.ctc.jobboard.sae.domain.QuestionAnswer;
import com.ctc.jobboard.sae.domain.Screen;
import com.ctc.jobboard.sae.domain.SeekScreenIdConfig;
import com.ctc.jobboard.sae.domain.screen.Answer;
import com.ctc.jobboard.sae.domain.screen.Question;
import com.ctc.jobboard.sae.utils.SAEConfig;

public class SAEScreensResponseHandler implements ResponseHandler<QuestionAnswer> {
	
	static Logger logger = Logger.getLogger(SAEScreensResponseHandler.class);
	
	
	private JobBoard jobBoard;
	private List<QuestionAnswer> questionAnswers = new ArrayList<QuestionAnswer>();
	
	public SAEScreensResponseHandler( JobBoard jobBoard){
		this.jobBoard = jobBoard;
	}

	
	@Override
	public JobBoard getJobBoard() {
		
		return jobBoard;
	}

	@Override
	public List<QuestionAnswer> getResult() {
		return questionAnswers;
	}

	@Override
	public void setJobBoard(JobBoard jobboard) {
		this.jobBoard = jobboard;
	}


	@Override
	public void handle(JBConnection jbConnection,
			BasicResponsePackage responsePackage) {
		
		if(responsePackage == null){
			return;
		}
		
		SAEScreensResponsePackage screenPackage = (SAEScreensResponsePackage) responsePackage;
		
		if(screenPackage.getResponse() != null){
			String screenId = screenPackage.getResponse().getId();
			Question[] qs = screenPackage.getResponse().getQuestion();
			if(qs != null){
				for(Question q : qs){
					
					String questionId = q.getId();
					
					String answer	= getSelectedAnswersFromSeekScreen(q);
					logger.debug("\tquestion: "+screenId + " - " + questionId+" - " +  answer);
					questionAnswers.add(new QuestionAnswer(screenId, questionId, answer));	
					
				}
			}
		}
	}

	
	
	
	private String getSelectedAnswersFromSeekScreen(Question q){
		if(q == null || q.getAnswer() == null){
			return "";
		}
		
		String value = "";
		for(Answer a : q.getAnswer()){
			if("true".equalsIgnoreCase(a.getSelected())){
				if(value == ""){
					value = a.getContent();
				}else{
					value +=";" + a.getContent();
				}
			}
		}
		
		return value;
		
	}
}
